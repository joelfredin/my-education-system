#pragma once

#include<iostream>
#include<string>
#include<vector>
#include "Student.h"

class Building
{
private:
    std::string name;
    std::string department;

public:
    Building(std::string name, std::string department)
    {
        this->name = name;
        this->department = department;
    }

};

class Campus
{
private:
    std::string name;
    std::vector<Building> departments;
    std::vector<Student> all_students;
public:
    Campus(std::string name)
    {
        this->name = name;
    }
    void add_students(Student student)
    {
        all_students.push_back(student);
    }
    void add_N_students(int N)
    {
        std::cout << "add something" << std::endl;
        for(int i = 0; i < N; i++)
        {
            std::string firstname;
            std::string lastname;
            std::cin >> firstname;
            std::cin >> lastname;
            Student student(firstname, lastname,i);

            add_students(student);
        }
    }
};

class School
{
private:
    std::string name;
    std::vector<Campus> campuses;
    std::vector<Student> all_students;
    
public:
    School(std::string name, std::vector<Campus> campuses)
    {
        this->name = name;
        this->campuses = campuses;
    }
    void construct_campus(int i)
    {

    }

};