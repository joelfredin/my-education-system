//#include "Campus.h"
#include <cassert>
#include<iostream>
#include<string>
#include <iomanip>
#include<fstream>
//#include "Person.h"
#include "Library.h"
#include "./hps/src/hps.h"
#include "Student.h"
#include "Course.h"
#include "Teacher.h"
#include "Campus.h"


/*
std::string get_person(int id)
{
    // Search through every possible id, when you find an index, return name

    return "dsa";
}

Course remove_from_course(Course course, std::string name)
{
    return course;
}
*/


void print_welcome_message()
{
    std::cout << "Welcome to the super exciting EDUCATIONAL SYSTEM!" << std::endl;
    std::cout << "Let us begin by creating two classes" << std::endl << std::endl;
}

std::vector<Course> construct_courses(int N)
{
    std::string name;
    std::string field_of_study;
    std::vector<Course> course_list;
    for(int i = 0; i < N; i++)
    {
        std::cout << "Please, tell me what the name of the course number " + std::to_string(i) + "!" << std::endl;
        std::cin >> name;
        std::cout << std::endl << "Please, tell me what field the course will be in." << std::endl;
        std::cin >> field_of_study;
        Course course(name, field_of_study);
        course_list.push_back(course);

    }
    return course_list;
}

std::vector<std::pair<std::string, std::string>> construct_course_properties(std::vector<Course> list_of_courses)
{
    std::vector<std::pair<std::string, std::string>> course_attributes;
    for(Course course : list_of_courses)
    {
        course_attributes.push_back({course.return_name(), course.return_field_of_study()});
    }
    return course_attributes;
}

void write_file(std::vector<std::pair<std::string, std::string>> some_data)
{
    //std::vector<std::pair<std::string, int>> data = {pair1, pair2};

    std::string serialized = hps::to_string(some_data);
    std::cout << "Writing " << serialized.size() << " Bytes" << std::endl;

    std::ofstream dataFile;
    dataFile.open("student.txt", std::ios::binary | std::ios::app);
    dataFile << serialized <<std::endl;
    dataFile.close();
}

void read_student_file()
{
    std::vector<std::pair<std::string, std::string>> data;

    std::string line;

    std::ifstream dataFile;

    std::string filePath = "student.txt";

    dataFile.open(filePath, std::ios::binary);
    if(dataFile.is_open())
    {
        std::string dataRead;
        while(getline(dataFile, line))
        {
      // should only be one line to read!
            data = hps::from_string<std::vector<std::pair<std::string, std::string>>>(line);
        }
        dataFile.close();
    }
    else
    {
        std::cout << "File read error!" <<std::endl;
    }

    for(std::pair<std::string, std::string> entry : data)
    {
        std::cout << entry.first << " | " << entry.second << std::endl;
    }
}

void read_teacher_file()
{
    std::vector<std::pair<std::string, int>> data;

    std::string line;

    std::ifstream dataFile;

    std::string filePath = "teacher.txt";

    dataFile.open(filePath, std::ios::binary);
    if(dataFile.is_open())
    {
        std::string dataRead;
        while(getline(dataFile, line))
        {
      // should only be one line to read!
            data = hps::from_string<std::vector<std::pair<std::string, int>>>(line);
        }
        dataFile.close();
    }
    else
    {
        std::cout << "File read error!" <<std::endl;
    }

    for(std::pair<std::string, int> entry : data)
    {
        std::cout << entry.first << " | " << entry.second << std::endl;
    }
}

void read_course_file()
{
    std::vector<std::pair<std::string, int>> data;

    std::string line;

    std::ifstream dataFile;

    std::string filePath = "course.txt";

    dataFile.open(filePath, std::ios::binary);
    if(dataFile.is_open())
    {
        std::string dataRead;
        while(getline(dataFile, line))
        {
      // should only be one line to read!
            data = hps::from_string<std::vector<std::pair<std::string, int>>>(line);
        }
        dataFile.close();
    }
    else
    {
        std::cout << "File read error!" <<std::endl;
    }

    for(std::pair<std::string, int> entry : data)
    {
        std::cout << entry.first << " | " << entry.second << std::endl;
    }
}



int main()
{
    //read_teacher_file();
    char input;
    int second_input;
    int third_input;

    std::string course_name;
    std::string course_field_of_study;

    std::vector<Course> all_courses;
    std::cout << "Welcome to the educational system, here I am sure you will have a lot of fun! You will here have the chance to control your own educational system!" << std::endl << std::endl;
    while(true)
    {
        std::cout << "I will now present you with the possible choices. Press the letter to the left, to explore what's on the right:" << std::endl;
        std::cout << std::left << std::setfill('.') << std::setw(20) << "\'C\'" << std::right << std::setfill('.') << std::setw(20) << " To handle courses" << std::endl;
        std::cout << std::left << std::setfill('.') << std::setw(20) << "\'S\'" << std::right << std::setfill('.') << std::setw(20) << " To handle students" << std::endl;
        std::cout << std::left << std::setfill('.') << std::setw(20) << "\'T\'" << std::right << std::setfill('.') << std::setw(20) << " To handle teachers" << std::endl;
        std::cout << std::left << std::setfill('.') << std::setw(20) << "\'I\'" << std::right << std::setfill('.') << std::setw(20) << " To show the information of students, teachers and courses" << std::endl;
        std::cout << std::left << std::setfill('.') << std::setw(20) << "\'L\'" << std::right << std::setfill('.') << std::setw(20) << " To discover the library" << std::endl;
        std::cout << std::left << std::setfill('.') << std::setw(20) << "\'Q\'" << std::right << std::setfill('.') << std::setw(20) << " To quit the program" << std::endl << std::endl;
        std::cin >> input;
        std::cout << std::endl << std::endl;
        if(input == 'C')
        {
            std::cout << "Ahh, I see that you are interested in courses. Would you like to construct a new course, or add students to courses?" << std::endl;
            std::cout << std::left << std::setfill('.') << std::setw(20) << "\'1\'" << std::right << std::setfill('.') << std::setw(20) << "To construct a new course" << std::endl;
            std::cout << std::left << std::setfill('.') << std::setw(20) << "\'2\'" << std::right << std::setfill('.') << std::setw(20) << " To add students to a course" << std::endl;
            std::cout << std::left << std::setfill('.') << std::setw(20) << "\'3\'" << std::right << std::setfill('.') << std::setw(20) << " To add teachers to the course" << std::endl;
            std::cout << std::left << std::setfill('.') << std::setw(20) << "\'4\'" << std::right << std::setfill('.') << std::setw(20) << " If you want to serialize the course information" << std::endl;
            std::cout << std::left << std::setfill('.') << std::setw(20) << "\'5\'" << std::right << std::setfill('.') << std::setw(20) << " If you want to display the information of the serialized file" << std::endl;
            std::cin >> second_input;
            if(second_input == 1)
            {
                std::cout << "Please, tell me the name of the course" << std::endl;
                std::cin >> course_name;
                std::cout << std::endl << "Now, tell me which area the course belongs to:" << std::endl;
                std::cin >> course_field_of_study;
                Course course(course_name, course_field_of_study);
                all_courses.push_back(course);
            }
            if(second_input == 2)
            {
                std::cout << "Which course would you like to add students to?" << std::endl << std::endl;
                std::cin >> third_input;
                std::cout << "Now, tell me how many students we should add:" << std::endl;
                std::cin >> second_input;
                all_courses[third_input].add_N_students(second_input);
            }
            if(second_input == 3)
            {
                std::cout << "Which course would you like to add teachers to?" << std::endl << std::endl;
                std::cin >> third_input;
                std::cout << "Now, tell me how many teachers we should add:" << std::endl;
                std::cin >> second_input;
                all_courses[third_input].add_N_students(second_input);
            }
            if(second_input == 4)
            {
                write_file(construct_course_properties(all_courses));
            }
            if(second_input == 5)
            {
                read_student_file();
            }

        }
        
        if(input == 'S')
        {

        }
        
        if(input ==  'T')
        {

        }
        
        if(input == 'I')
        {

        }

        if(input == 'L')
        {
            
        }

        if(input == 'Q')
        {
            std::cout << "Goodbye, and take care!!" << std::endl;
            break;
        }


    }

    for(Course course : all_courses)
    {
        std::cout << course.return_name() << std::endl;
    }
    Library lib1("great");

    lib1.add_collection_of_books(2);
    lib1.print_books();













    /*
    std::cout << "Hello, and welcome to the education system. Let us begin by constructing a course" << std::endl;
    std::vector<Course> course_list = construct_courses(1);
    std::cout << "Let us now add a few students to this course" << std::endl;
    course_list.back().add_N_students(3);
    std::vector<Student> student_list = course_list.back().return_students();

    for(Student student : student_list)
    {
        std::cout << student.first_name();
    }

        //std::cout << course.return_students()[0].first_name();
        std::vector<std::pair<std::string, std::string>> vector_of_student_attributes;
        //std::vector<Student> student_list = construct_list_of_students()
        for(Student student : student_list)
        {
            std::string a = student.first_name();
            std::string b = student.last_name();
            vector_of_student_attributes.push_back({a, b});
        }

        std::string serialized = hps::to_string(vector_of_student_attributes);
        std::cout << "Writing " << serialized.size() << " Bytes" << std::endl;
        std::ofstream dataFile;
        dataFile.open("student.txt", std::ios::binary | std::ios::app);
        dataFile << serialized <<std::endl;
        dataFile.close();


        std::vector<std::pair<std::string, int>> data_student;

        std::string line_student;

        std::ifstream dataFile_student;

        std::string filePath__student = "student.txt";

        dataFile.open(filePath__student, std::ios::binary);
        if(dataFile_student.is_open())
        {
            std::string dataRead_student;
            while(getline(dataFile_student, line_student))
            {
            // should only be one line to read!
            data_student = hps::from_string<std::vector<std::pair<std::string, int>>>(line_student);
            }
            dataFile.close();
        }
        else
        {
            std::cout << "File read error!" <<std::endl;
        }

        for(std::pair<std::string, int> entry : data_student)
        {
            std::cout << entry.first << " | " << entry.second << std::endl;
        }


*/






    
    /*
    std::cout << "Hello and welcome to a fantastic educational system! In this application we will construct our own UNIVERSITY!" << std::endl;
    std::cout << "Let us begin by choosing a new for our university. So what do you want the name to be?" << std::endl;
    std::string choice;
    int choice2;
    std::cin >> choice;
    //School our_school(choice);
    std::cout << "Ah, what a perfect choice to name the school " + choice + "!" << std::endl;
    std::cout << "Now we want to start to add some campuses to this school! Please type a number, how many campuses you want to add" << std::endl;
    std::cin >> choice2;
    std::cout << std::endl << "Ok, let us add " + std::to_string(choice2) + " students!";
    std::vector<Campus> campus1;
    /*
    for(int i = 0; i < choice2; i++)
    {

    }
    */
/*
    while(true)
    {
        std::cout << "Do you want to exit this application? If you want, type \'YES\' and hit enter:" << std::endl;
        std::cin >> choice;
        if(choice == "YES")
        {
            break;
        }
        std::cout << "Now, pick a campus which you want to take a closer look at" << std::endl;
    }
    char input;
    Person hello("Joel", "Sandro");
    std::cout << hello.first_name() << std::endl;
    std::cout << hello.last_name() << std::endl;
    std::cout << "Do you want to borrow a book from the library?" << std::endl << std::endl;
    Book book("my", "haha");
    Library library("dd", {});
    library.order_books(book);
    library.print_elements();

    std::cin >> input;
    if(input == 'Y')
    {
        
    }
*/
    return 0;
}