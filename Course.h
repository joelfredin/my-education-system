#pragma once

#include<iostream>
#include<string>
#include<vector>
#include "Student.h"

// Creates a class representing a course.
class Course
{
private:
    std::string course_name;
    std::string field_of_study;
    std::vector<Student> students_in_course;

public:
    Course(std::string course_name, std::string field_of_study)
    {
        this->course_name = course_name;
        this->field_of_study = field_of_study;
    }
    std::string return_name(){
        return course_name;
    }
    std::string return_field_of_study(){
        return field_of_study;
    }
    std::vector<Student> return_students(){
        return students_in_course;
    }
    void add_students(Student student)
    {
        students_in_course.push_back(student);
    }
    void add_N_students(int N)
    {
        std::cout << "add something" << std::endl;
        for(int i = 0; i < N; i++)
        {
            std::string firstname;
            std::string lastname;
            std::cin >> firstname;
            std::cin >> lastname;
            Student student(firstname, lastname,i);

            add_students(student);
        }
};
/*
class StudentWriter
{
private:
    Course course;

public:
    StudentWriter(Course course)
    {
        this->course = course;
    }
*/
    /*
    void print_info()
    {
        for(Student student : course.return_students())
        {
            std::cout << student.first_name + " " + student.last_name << std::endl;
        }
    }
    */
};