#pragma once

#include<iostream>
#include<string>

class Person
{
protected:
    std::string firstName;
    std::string lastName;

public:
    Person(std::string firstName, std::string lastName)
    {
        this->firstName = firstName;
        this->lastName = lastName;
    }
    std::string first_name()
    {
        return firstName;
    }
    std::string last_name()
    {
        return lastName;
    }

};

