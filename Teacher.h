#pragma once
#include<vector>
#include "Course.h"
#include "Person.h"

class Teacher : Person {
  private:
  	std::string department;
    int id;
    std::vector<Course> list_of_courses;
  public:
  	Teacher(std::string firstName, std::string lastName, std::string department, int id)
    :Person(firstName, lastName){
      this->department = department;
      this->id = id;
  	}
    void add_course(Course course)
    {
        list_of_courses.push_back(course);
    }
    
    void add_N_courses(int N)
    {
        std::cout << "add something" << std::endl;
        for(int i = 0; i < N; i++)
        {
            std::string name;
            std::string field_of_study;
            std::cin >> name;
            std::cin >> field_of_study;
            Course course(name, field_of_study);

            add_course(course);
        }
    }
        
    /*
    std::string GetName(){
  		return firstName + " " + lastName;
  	}
    */
    
};